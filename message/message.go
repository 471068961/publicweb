package message

import (
	"github.com/jinzhu/gorm"
)

// User 表示一个用户的信息
type User struct {
	gorm.Model        // gorm的模型
	Username   string `gorm:"unique;not null"` // 用户名
	Password   string // 密码
	Admin      bool   // 是否为管理员
	Address    string // 项目地址
	Time       int64  // 会员时长
}

// FileDest 用户文件存储的路径
var FileDest string = "./html/userFile/"
