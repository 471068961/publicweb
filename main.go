package main

import (
	"MyWeb2/message"
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// DB 全局MySQL连接
var DB *gorm.DB

func find(u *message.User) (admin bool, ok bool) {
	user := message.User{}
	DB.Where("username = ?", u.Username).Find(&user)
	if user.Username != u.Username || user.Password != u.Password {
		return false, false
	} else if !user.Admin {
		return false, true
	}
	return true, true
}

func handleStaticFile(r *gin.Engine) {
	// 加载静态文件
	r.Static("css", "./html/css")
	r.Static("fa", "./html/fa")
	r.Static("img", "./html/img")

	//加载文件
	r.LoadHTMLFiles("./html/login.html", "./html/index.html", "./html/user.html")
}

func insertUser(c *gin.Context) {
	admin, _ := strconv.ParseBool(c.PostForm("admin"))
	time, _ := strconv.ParseInt(c.PostForm("time"), 64, 64)
	user := message.User{
		Username: c.PostForm("username"),
		Password: c.PostForm("password"),
		Admin:    admin,
		Address:  c.PostForm("address"),
		Time:     time,
	}
	DB.Create(&user)
	os.Mkdir("./html/userFile"+user.Username, os.ModePerm)
}

func main() {
	db, err := gorm.Open("mysql", "root:dingwanli1011@(127.0.0.1:3306)/userInfo?charset=utf8mb4&parseTime=True")
	if err != nil {
		fmt.Println(err)
		// logger.Logger(err)
		return
	}

	db.AutoMigrate(&message.User{})
	DB = db
	// 关闭数据库连接
	defer DB.Close()

	// DB.Create(&message.User{
	// 	Username: "Admin",
	// 	Password: "abcd123",
	// 	Admin:    true,
	// 	Address:  "",
	// 	Time:     -1,
	// })

	r := gin.Default()
	handleStaticFile(r)
	r.GET("/admin", func(c *gin.Context) {
		c.HTML(http.StatusOK, "login.html", nil)
	})

	// 管理员登陆
	r.POST("/admin", func(c *gin.Context) {
		adminInfo := &message.User{
			Username: c.PostForm("adminName"),
			Password: c.PostForm("passWord"),
		}
		admin, ok := find(adminInfo)
		if !ok {
			// 信息错误返回登陆页面
			c.HTML(http.StatusOK, "login.html", gin.H{
				"Err": "你的账号或密码输入有误，请重新输入",
			})
		} else if ok && !admin {
			// 返回用户界面
			c.HTML(http.StatusOK, "user.html", nil)
		} else {
			// 返回管理员界面
			c.HTML(http.StatusOK, "admin.html", nil)
		}
	})

	// 用户登陆
	r.GET("/user", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})
	r.POST("/user", func(c *gin.Context) {
		userInfo := &message.User{
			Username: c.PostForm("adminName"),
			Password: c.PostForm("passWord"),
		}
		_, ok := find(userInfo)

		if !ok {
			// 信息错误返回登陆页面
			c.HTML(http.StatusOK, "index.html", gin.H{
				"Err": "你的账号或密码输入有误，请重新输入",
			})
		} else {
			// 返回用户界面
			c.HTML(http.StatusOK, "user.html", gin.H{
				"username": userInfo.Username,
			})
		}
	})
	r.POST("/user/file", func(c *gin.Context) {
		file, err := c.FormFile("file")
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintln("上传文件失败", err))
		} else {
			c.SaveUploadedFile(file, message.FileDest+c.PostForm("username")+"/"+file.Filename)
			c.String(http.StatusOK, message.FileDest+c.PostForm("username")+"/"+file.Filename)
			c.String(http.StatusOK, "ok")
		}
	})
	// 访问无路由时，返回主页
	r.NoRoute(func(c *gin.Context) {
		c.HTML(http.StatusNotFound, "index.html", nil)
	})
	r.Run(":8080")
}
