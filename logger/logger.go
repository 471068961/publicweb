package logger

import (
	"log"
	"os"
	"time"
)

// Logger 打印错误到文件
func Logger(err error) {
	if err != nil {
		fileName := "logger/" + time.Now().Format("2006/1/2") + ".txt"
		file, _ := os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, os.ModePerm)
		defer file.Close()
		log.SetOutput(file)
		log.Println(err)
	}
}
